## 项目说明
- 公共父类
- 统一版本管理
- 统一插件管控
- 统一规范管理

## 项目特点
- gientech-framework-common 公共模块
- gientech-framework-jpa 适合基于spring-data-jpa工程引用
- gientech-framework-mybatis 适合基于mybatis-plus工程引用
- 同代码生成器配合使用效果更好

## 公共功能说明
- Log：代码生成器同步生成相关注解，业务系统需要自己实现切面完成日志分析、搜集功能。
- CommonConfig：可配置参数，统一异常处理和mybatis完成sql信息打印
- SwaggerConfig、SwaggerParamsConfig：swagger配置信息
- Result：统一返回类型
- PageWrapper：统一分页包装类
- BaseStatus：统一返回状态

## 说明
- 目前设计功能均为公共功能
- 后续公司级定制化功能需公司同意后才会同步更新
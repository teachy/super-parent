package com.gientech.framework.common.exception;

import com.gientech.framework.common.enums.BaseStatus;
import lombok.Data;

import java.text.MessageFormat;

/**
 * warn exception
 *
 * @author gang.tu
 */
@Data
public class WarnException extends RuntimeException {

    private int code = BaseStatus.WARN.getCode();
    private String warnMsg;

    public WarnException() {
    }

    public WarnException(String warnMsg) {
        super(MessageFormat.format(BaseStatus.WARN.getMsg(), warnMsg));
    }

    public WarnException(String warnMsg, Throwable e) {
        super(MessageFormat.format(BaseStatus.WARN.getMsg(), warnMsg), e);
    }
}
package com.gientech.framework.common.exception;

import com.gientech.framework.common.enums.BaseStatus;
import lombok.Data;

/**
 * AuthFailedException
 *
 * @author gang.tu
 */
@Data
public class AuthFailedException extends RuntimeException {

    private int code = BaseStatus.AUTH_FAILED.getCode();

    public AuthFailedException() {
    }

    public AuthFailedException(String message) {
        super(message);
    }

    public AuthFailedException(String message, Throwable e) {
        super(message, e);
    }
}
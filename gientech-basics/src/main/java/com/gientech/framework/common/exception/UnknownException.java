package com.gientech.framework.common.exception;

import com.gientech.framework.common.enums.BaseStatus;
import lombok.Data;

import java.text.MessageFormat;

/**
 * Unknown exception
 * 尽量使用明确的异常
 *
 * @author gang.tu
 */
@Data
public class UnknownException extends RuntimeException {

    private int code = BaseStatus.FAILED.getCode();

    public UnknownException() {
    }

    public UnknownException(String message) {
        super(message);
    }

    public UnknownException(String message, Throwable e) {
        super(message, e);
    }


    public UnknownException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public UnknownException(BaseStatus status) {
        super(status.getMsg());
        this.code = status.getCode();
    }

    public UnknownException(BaseStatus status, Object... args) {
        super(MessageFormat.format(status.getMsg(), args));
        this.code = status.getCode();
    }
}
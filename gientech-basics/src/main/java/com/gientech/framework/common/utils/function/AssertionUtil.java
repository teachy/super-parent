package com.gientech.framework.common.utils.function;


import com.gientech.framework.common.exception.BusinessException;

/**
 * @author gang.tu
 * @ClassName AssertionUtil
 * @Description
 * @Date 2022/7/6 15:22
 */
public class AssertionUtil {

    /**
     * @description 断言结果是true，否则抛异常
     * @author gang.tu
     * @date 2022/7/6 15:22
     * @return: void
     */
    public static void AssertIsTrue(AssertObjectFunction assertObjectFunction, String errorMsg) {
        if (!assertObjectFunction.test()) {
            throw new BusinessException(errorMsg);
        }
    }

    /**
     * @description 断言结果是false，否则抛异常
     * @author gang.tu
     * @date 2022/7/6 15:22
     * @return: void
     */
    public static void AssertIsFalse(AssertObjectFunction assertObjectFunction, String errorMsg) {
        if (assertObjectFunction.test()) {
            throw new BusinessException(errorMsg);
        }
    }
}
package com.gientech.framework.common.utils.function;

@FunctionalInterface
public interface AssertObjectFunction {

    /**
     * @description 判断条件
     * @author gang.tu
     * @date 2022/7/6 15:39
     * @return: boolean
     */
    boolean test();

}

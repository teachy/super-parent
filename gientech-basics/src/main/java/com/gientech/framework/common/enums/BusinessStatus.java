package com.gientech.framework.common.enums;

/**
 * 操作状态
 * 
 * @author gang.tu
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}

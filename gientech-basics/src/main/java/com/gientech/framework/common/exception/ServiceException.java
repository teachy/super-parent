package com.gientech.framework.common.exception;

import com.gientech.framework.common.enums.BaseStatus;
import lombok.Data;

import java.text.MessageFormat;

/**
 * service exception
 *
 * @author gang.tu
 */
@Data
public class ServiceException extends RuntimeException {

    private int code = BaseStatus.SERVICE_EXCEPTION.getCode();
    private String serviceName;

    public ServiceException() {
    }

    public ServiceException(String serviceName) {
        super(MessageFormat.format(BaseStatus.SERVICE_EXCEPTION.getMsg(), serviceName));
    }

    public ServiceException(String serviceName, Throwable e) {
        super(MessageFormat.format(BaseStatus.SERVICE_EXCEPTION.getMsg(), serviceName), e);
    }
}
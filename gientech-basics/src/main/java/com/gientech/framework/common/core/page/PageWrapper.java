package com.gientech.framework.common.core.page;

import lombok.Data;

import java.util.List;

/**
 * @author gang.tu
 * @ClassName BasePage
 * @Date 2022/10/13 18:40
 */
@Data
public class PageWrapper<T> {
    public List<T> records;
    /**
     * total count
     */
    public Long totalCount = 0L;
    /**
     * page size
     */
    public Long pageSize = 10L;
    /**
     * current page
     */
    public Long currentPage = 0L;

    public Long totalPage = 0L;

    public static <T> PageWrapper<T> of(List<T> list, Long totalCount, Long pageSize, Long currentPage, Long totalPage) {
        PageWrapper<T> basePage = new PageWrapper<>();
        basePage.totalPage = totalPage;
        basePage.totalCount = totalCount;
        basePage.records = list;
        basePage.currentPage = currentPage;
        basePage.pageSize = pageSize;
        return basePage;
    }

    public static <T, E extends PageWrapper> PageWrapper<T> of(E e) {
        return of(e.records, e.getTotalCount(), e.getPageSize(), e.getCurrentPage(), e.getTotalPage());
    }
}
package com.gientech.framework.common.enums;

import java.text.MessageFormat;
import java.util.Locale;

/**
 * status enum
 *
 * @author gang.tu
 */
public enum BaseStatus {

    SUCCESS(200, "success", "成功"),
    AUTH_FAILED(401, "authentication failed", "认证失败"),
    VALIDATION_EXCEPTION(408, "{0}", "{0}"),
    FAILED(500, "{0}", "{0}"),
    SERVICE_EXCEPTION(505, "Service not available:{0}", "服务不可用：{0}"),
    BUSINESS_EXCEPTION(506, "{0}", "{0}"),
    WARN(10001, "{0}", "{0}");


    private final int code;
    private final String enMsg;
    private final String zhMsg;

    BaseStatus(int code, String enMsg, String zhMsg) {
        this.code = code;
        this.enMsg = enMsg;
        this.zhMsg = zhMsg;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg(String language) {
        if (Locale.SIMPLIFIED_CHINESE.getLanguage().equals(language)) {
            return this.zhMsg;
        } else {
            return this.enMsg;
        }
    }

    public String getMsg() {
        return this.zhMsg;
    }

    public String getMsg(Object... messages) {
        return MessageFormat.format(this.getMsg(), messages);
    }

}

package com.gientech.framework.common.exception;

import com.gientech.framework.common.enums.BaseStatus;
import lombok.Data;

import java.text.MessageFormat;

/**
 * business exception
 *
 * @author gang.tu
 */
@Data
public class BusinessException extends RuntimeException {

    private int code = BaseStatus.BUSINESS_EXCEPTION.getCode();
    private String errorMsg;

    public BusinessException() {
    }

    public BusinessException(String errorMsg) {
        super(MessageFormat.format(BaseStatus.BUSINESS_EXCEPTION.getMsg(), errorMsg));
    }

    public BusinessException(String errorMsg, Throwable e) {
        super(MessageFormat.format(BaseStatus.BUSINESS_EXCEPTION.getMsg(), errorMsg), e);
    }
}
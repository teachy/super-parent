package com.gientech.framework.common.core.domain;

import com.gientech.framework.common.constant.Constants;
import com.gientech.framework.common.enums.BaseStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回数据
 *
 * @author gang.tu
 */
@Data
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    private String message = Constants.SUCCESS;
    private int code = BaseStatus.SUCCESS.getCode();
    private T data;

    public Result() {
        super();
    }

    public Result(int code) {
        super();
        this.code = code;
    }

    public Result(T data) {
        super();
        this.data = data;
    }

    public Result(T data, String msg) {
        super();
        this.data = data;
        this.message = msg;
    }

    public Result(T data, String msg, int code) {
        super();
        this.data = data;
        this.message = msg;
        this.code = code;
    }

    public Result(Throwable e) {
        super();
        this.message = e.getMessage();
        this.code = BaseStatus.FAILED.getCode();
    }


    public void setData(T data) {
        this.data = data;
    }

    public static <T> Result<T> error() {
        return error(BaseStatus.FAILED.getCode(), "未知异常，请联系管理员");
    }

    public static <T> Result<T> error(String msg) {
        return error(BaseStatus.FAILED.getCode(), msg);
    }

    public static <T> Result<T> error(int code, String msg) {
        Result<T> result = new Result<>();
        result.setCode(code);
        result.setMessage(msg);
        return result;
    }

    public static <T> Result<T> ok(String msg) {
        Result<T> result = new Result<>();
        result.setMessage(msg);
        return result;
    }

    public static <T> Result<T> ok(String msg, T data) {
        Result<T> result = new Result<>();
        result.setMessage(msg);
        result.setData(data);
        return result;
    }

    public static <T> Result<T> ok(T data) {
        Result<T> result = new Result<>();
        result.setData(data);
        return result;
    }

    public static <T> Result<T> ok() {
        return new Result<>();
    }

    public static <T> Result<T> success() {
        return ok();
    }

    public static <T> Result<T> success(T data) {
        Result<T> result = ok();
        result.setData(data);
        return result;
    }

}

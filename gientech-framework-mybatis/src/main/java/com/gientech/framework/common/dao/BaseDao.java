package com.gientech.framework.common.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 基础Dao
 *
 * @author gang.tu
 */
public interface BaseDao<T> extends BaseMapper<T> {

}

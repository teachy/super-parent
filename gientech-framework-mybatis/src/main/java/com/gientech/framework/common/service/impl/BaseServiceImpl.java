package com.gientech.framework.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gientech.framework.common.query.Query;
import com.gientech.framework.common.service.BaseService;
import org.apache.commons.lang3.StringUtils;


/**
 * 基础服务类，所有Service都要继承
 *
 * @author gang.tu
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<T> {

    /**
     * 获取分页对象
     *
     * @param query 分页参数
     */
    protected IPage<T> getPage(Query query) {
        Page<T> page = new Page<>(query.getCurrentPage(), query.getPageSize());
//        page.addOrder(OrderItem.desc("id"));
        return page;
    }

    protected QueryWrapper<T> getWrapper(Query query) {
        QueryWrapper<T> wrapper = new QueryWrapper<>();
        wrapper.like(StringUtils.isNotBlank(query.getUserId()), "userId", query.getUserId());
        wrapper.eq(StringUtils.isNotBlank(query.getProjectId()), "projectId", query.getProjectId());
        wrapper.like(StringUtils.isNotBlank(query.getWorkspaceId()), "workspaceId", query.getWorkspaceId());
        return wrapper;
    }

}
package com.gientech.framework.common.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 基础服务接口，所有Service接口都要继承
 *
 * @author gang.tu
 */
public interface BaseService<T> extends IService<T> {


}
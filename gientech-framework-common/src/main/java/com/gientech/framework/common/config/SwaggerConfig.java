package com.gientech.framework.common.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author gang.tu
 * @ClassName SwaggerParamsConfig
 * @Description
 * @Date 2022/10/19 19:45
 */
@Configuration
@EnableSwagger2
@ConditionalOnMissingBean(Docket.class)
@Profile({ "!prod & !PROD" })
public class SwaggerConfig {
    @Autowired
    private SwaggerParamsConfig swaggerParamsConfig;


    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerParamsConfig.getTitle())
                .description(swaggerParamsConfig.getDescription())
                .termsOfServiceUrl(swaggerParamsConfig.getTermsOfServiceUrl())
                .version(swaggerParamsConfig.getVersion())
                .build();
    }

}
package com.gientech.framework.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author gang.tu
 * @ClassName CommonConfig
 * @Date 2022/10/14 14:29
 */
@Data
@Component("baseCommonConfig")
@ConfigurationProperties(prefix = "common")
public class CommonConfig {
    /**
     * @description 控制统一异常处理是否生效
     */
    private boolean enableGlobalControllerAdvice = true;
    /**
     * @description 打印完整sql是否生效
     */
    private boolean printSqlInterceptor = true;
}
package com.gientech.framework.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author gang.tu
 * @ClassName SwaggerParamsConfig
 * @Description
 * @Date 2022/10/13 19:45
 */
@Data
@Component
@ConfigurationProperties(prefix = "swagger")
public class SwaggerParamsConfig {
    private String title;
    private String description;
    private String termsOfServiceUrl;
    private String version;
}